//Charles Justin Reusnow • CS 2261-001

//A Receiver object is an object that extends the Communicator object, but intended to receive and decrypt messages from a Sender object.
class Receiver extends Communicator {
    //No additional Member Variables needed.

    //Constructor
    public Receiver(String name) {
        this.name = name;
        this.key = null; //Must be received from Sender first.
        this.message = null; //Must be received from Sender first.
    }

    //Getters handled in Communicator class.

    //Setters handled in Communicator class.

    //To String
    public String toString() {
        return super.toString();
    }

    //Member Methods
    //"Receives" the encryption key from a Sender.
    void receiveKey(String key) {
        this.key = key;
    }

    //"Receives" the encrypted message from a Sender.
    void receiveMessage(Message message) {
        this.message = message;
    }

    //Calls the key verification method in the Message class and returns the result.
    boolean verifyKey() {
        return this.message.verifyKey(this.key);
    }

    //Calls the decryption method in the Message class to decrypt the content of the Message.
    void decrypt() {
        this.message.decrypt(this.key);
    }
}
