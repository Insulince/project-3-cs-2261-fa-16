//Charles Justin Reusnow • CS 2261-001

//Communicator stores the general methods and members that a Sender and Receiver object both share. A form of polymorphism. This class will never be instantiated, for every "Communicator" is either a "Sender" or a "Receiver", thus it is abstract.
abstract class Communicator {
    //Member Variables
    String name; //The name of this communicator.
    String key; //Its encryption key.
    Message message; //Its message object.

    //No Constructor needed.

    //Getters
    public String getName() {
        return this.name;
    }

    public String getKey() {
        return this.key;
    }

    public Message getMessage() {
        return this.message;
    }

    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    //To String
    public String toString() {
        return "Communicator{" +
                "name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", message=" + message +
                '}';
    }

    //No Member Methods needed.
}
