//Charles Justin Reusnow • CS 2261-001

//Imports
import java.util.Scanner;

import static java.lang.System.exit;

//This is the driver class of the program. It is never instantiated, just merely used as a platform to execute and showcase the other objects.
public class Application {
    //Final Variables
    private final static Scanner SCANNER = new Scanner(System.in); //The scanner for input.
    private final static int SLEEP_DURATION = 500; //How long "timed output" should wait before displaying.
    private final static int MSG_INVALID_ENCRYPTION_KEY = -999; //Error code for when the program exits because of an invalid encryption key
    private final static int MSG_THREAD_SLEEP_FAILURE = -998; //Error code for when the program exits because of a failure to execute Thread.Sleep

    //Global Variables
    private static Sender sender; //The Sender object.
    private static Receiver receiver; //The Receiver object.

    //Main Method
    public static void main(String[] args) {
        introduction(); //Begin the program with a light introduction.
        while (repeat()) { //While the user wishes to repeat...
            gatherInfo(); //Gather information about the Messages, Senders, Receivers, etc.
            encrypt(); //Encrypt the message.
            send(); //Send the message to the receiver.
            verify(); //Verify the key is valid.
            decrypt(); //Decrypt the message.
            await("\nThis completes the encryption and decryption process.\n");
            System.out.println("=======================================================================================================================================================================\n");
        }

        System.out.println("\nThank you for your time!");

        SCANNER.close();
    }

    //Functions
    //Light introduction to the program.
    private static void introduction() {
        art(); //ASCII art introduction.

        System.out.println("\nWelcome to The Encryptographer 2000™!");
        await("Press enter to step through this program.\n");
    }

    //Checks if the user wants to repeat the encryption process.
    private static boolean repeat() {
        String response;

        response = await("Would you like to go through the encryption process with a new message (y/n)?\n").toLowerCase(); //Prompt user.

        while (!response.equals("y") && !response.equals("n")) { //If invalid response...
            System.out.println("ERROR: Unrecognized response! Please enter 'y' or 'n'!"); //Warn user.
            response = await("Would you like to send a message (y/n)?\n"); //Prompt user.
        }

        return response.equals("y");
    }

    //Gathers relevant information for the encryption process.
    private static void gatherInfo() {
        String senderName;
        String receiverName;
        String messageContent;

        await("\n1. GATHER INFORMATION\n");

        //Get Sender Name
        senderName = await("First enter the name of the Sender:\n"); //Prompt user.
        while (senderName.trim().equals("")) { //While invalid name...
            await("ERROR: Invalid name! Names must be at least one non-space character long!"); //Warn user.
            senderName = await("Please re-enter the name of the Sender:\n"); //Prompt user.
        }

        //Get Receiver Name
        receiverName = await("Now enter the name of the Receiver:\n");
        while (receiverName.trim().equals("")) {
            await("ERROR: Invalid name! Names must be at least one non-space character long!");
            receiverName = await("Please re-enter the name of the Receiver:\n");
        }

        //Get Message Content
        messageContent = await("Now enter the message to be encrypted:\n");
        while (messageContent.trim().equals("")) {
            await("ERROR: Invalid message! Messages must be at least one non-space character long!");
            messageContent = await("Please re-enter the message to be encrypted:\n");
        }

        sender = new Sender(senderName, new Message(messageContent)); //Create the Sender object and its Message object.
        receiver = new Receiver(receiverName); //Create the Receiver object.

        await("Great, next " + sender.getName() + " will encrypt the message.\n");
    }

    //Encrypts the Message.
    private static void encrypt() {
        await("2. ENCRYPT\n");

        timedOutput(sender.getName() + " is generating the encryption key...", 1);
        sender.generateKey(); //Generate the encryption key.
        await(" done.");
        timedOutput(sender.getName() + " is encrypting the message...", 1);
        sender.encrypt(sender.getKey()); //Encrypt the Message.
        await(" done.");

        await("\nThe message is now encrypted as:\n" + sender.getMessage().getEncryptedContent() + "\nAnd the encryption key is:\n" + sender.getKey());
        await("Next " + sender.getName() + " will send the message to " + receiver.getName() + ".\n");
    }

    //Sends the Message to a Receiver.
    private static void send() {
        await("3. SEND\n");

        timedOutput(sender.getName() + " is sending the encryption key to " + receiver.getName() + "...", 1);
        sender.sendKey(receiver); //Send the encryption key to the Receiver.
        await(" done.");
        timedOutput(sender.getName() + " is sending the encrypted message to " + receiver.getName() + "...", 1);
        sender.sendMessage(receiver); //Send the encrypted message to the receiver.
        await(" done.");

        await("\n" + receiver.getName() + " has received the encryption key and encrypted message, next he/she will verify the key that was sent with the message is valid.\n");
    }

    //Verifies the encryption key is valid.
    private static void verify() {
        await("4. VERIFY KEY\n");

        timedOutput(receiver.getName() + " is verifying the validity of the encryption key...", 1);
        if (receiver.verifyKey()) { //If it is a valid encryption key...
            await(" done.");
            await("\n" + receiver.getName() + " has determined that the key is valid, so now he/she will decrypt the message with it.\n");
        } else { //If it is an invalid encryption key...
            await(" done.");
            await("\n" + receiver.getName() + " has determined that the key is invalid, program exiting.");
            exit(MSG_INVALID_ENCRYPTION_KEY); //Exit.
        }
    }

    //Decrypts the Message.
    private static void decrypt() {
        await("5. DECRYPT\n");

        timedOutput(receiver.getName() + " is now decrypting the message...", 1);
        receiver.decrypt(); //Decrypt the Message.
        await(" done.");

        await("\n" + receiver.getName() + " has decrypted the message into:\n" + receiver.getMessage().getDecryptedContent());
    }

    //Await is a function which forces the program to wait for the user to press enter, or type in some input. This one is for if you want the program to wait silently (it's chained to the next function below).
    private static String await() {
        return await("");
    }

    //Waits with a prompt for the user to press enter or enter some input.
    private static String await(String prompt) {
        System.out.print(prompt); //Prompt.
        return SCANNER.nextLine(); //Wait.
    }

    //Timed Ouput is a function which forces the program to wait a fixed amount of time. This is used to give the appearance of "thinking" time, by the program, to make the process clearer. This one is used to always print a new line (it's chained to the next function below).
    private static void timedOutput(String prompt) {
        timedOutput(prompt, 0);
    }

    //Waits a fixed amount of time and could print on a new line or the same line, depending on "mode" parameter.
    private static void timedOutput(String prompt, int mode) {
        if (mode == 0) { //If it should print on its own line...
            System.out.println(prompt); //Prompt.
        } else { //If you should print on the current line...
            System.out.print(prompt); //Prompt.
        }

        try {
            Thread.sleep(SLEEP_DURATION); //Wait.
        } catch (InterruptedException e) { //This try-catch is required when you use Thread.Sleep.
            exit(MSG_THREAD_SLEEP_FAILURE); //Exit with the appropriate exit code.
        }
    }

    //Output this totally relevant ASCII art. Note the trade mark. Its official.
    private static void art() {
        System.out.println(
                "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````\n" +
                        "```THE`````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````\n" +
                        "``````##############````####```````###````##############````############`````###`````````###````#############````###############````#############``````````````````````\n" +
                        "`````###############```######``````###```###############```##############````###`````````###```###############```###############```###############`````````````````````\n" +
                        "`````###```````````````#######`````###```####``````````````###`````````###```###`````````###```###`````````###`````````###`````````###`````````###`````````````````````\n" +
                        "`````###```````````````###`####````###```###```````````````###`````````###```###`````````###```###`````````###`````````###`````````###`````````###`````````````````````\n" +
                        "`````###############```###``####```###```###```````````````##############````###############```###############`````````###`````````###`````````###`````````````````````\n" +
                        "`````###############```###```####``###```###```````````````#############``````##############```##############``````````###`````````###`````````###`````````````````````\n" +
                        "`````###```````````````###````####`###```###```````````````###``````###``````````````````###```###`````````````````````###`````````###`````````###`````````````````````\n" +
                        "`````###```````````````###`````#######```####``````````````###```````###`````````````````###```###`````````````````````###`````````###`````````###`````````````````````\n" +
                        "`````###############```###``````######```###############```###````````###````###############```###`````````````````````###`````````###############`````````````````````\n" +
                        "``````##############```###```````####`````##############```###`````````###```##############````###`````````````````````###``````````#############``````````````````````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````\n" +
                        "`````````````````````````````````##############```#############```````###########``````#############````###`````````###````##############````############``````````````\n" +
                        "````````````````````````````````###############```##############`````#############````###############```###`````````###```###############```##############`````````````\n" +
                        "````````````````````````````````###```````````````###`````````###```####```````####```###`````````###```###`````````###```###```````````````###`````````###````````````\n" +
                        "````````````````````````````````###```````````````###`````````###```###`````````###```###`````````###```###`````````###```###```````````````###`````````###````````````\n" +
                        "``````````````###############```###```########````##############````###############```###############```###############```###############```##############`````````````\n" +
                        "``````````````###############```###```#########```#############`````###############```##############````###############```###############```#############``````````````\n" +
                        "````````````````````````````````###`````````###```###``````###``````###`````````###```###```````````````###`````````###```###```````````````###``````###```````````````\n" +
                        "````````````````````````````````###`````````###```###```````###`````###`````````###```###```````````````###`````````###```###```````````````###```````###``````````````\n" +
                        "````````````````````````````````###############```###````````###````###`````````###```###```````````````###`````````###```###############```###````````###`````````````\n" +
                        "`````````````````````````````````#############````###`````````###```###`````````###```###```````````````###`````````###````##############```###`````````###````````````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````#####`#####`#####`#####`™```\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````#`#```#`#```#`#```#`````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````#####`#```#`#```#`#```#`````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````#`````#```#`#```#`#```#`````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````#####`#####`#####`#####`````\n" +
                        "```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````");
    }
}