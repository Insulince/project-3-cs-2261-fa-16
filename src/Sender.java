//Charles Justin Reusnow • CS 2261-001

//A Sender is an object that extends the Communicator object, but intended to encrypt and send a message to a Receiver object.
class Sender extends Communicator {
    //No additional member variables needed.

    //Constructor
    public Sender(String name, Message message) {
        this.name = name;
        this.key = null; //The key has to be generated first.
        this.message = message;
    }

    //Getters handled in Communicator class.

    //Setters handled in Communicator class.

    //To String
    public String toString() {
        return super.toString();
    }

    //Member Methods
    //Calls the key generation method in the Message class and stores the result here.
    void generateKey() {
        this.key = this.message.generateKey();
    }

    //Calls the encryption method in the Message class to encrypt the content of the Message.
    void encrypt(String key) {
        this.message.encrypt(key);
    }

    //"Sends" the encryption key to the receiver.
    void sendKey(Receiver receiver) {
        receiver.receiveKey(this.key);
    }

    //"Sends" the encrypted message to the receiver.
    void sendMessage(Receiver receiver) {
        receiver.receiveMessage(new Message(this.message.getEncryptedContent())); //Creates a new message so there is no leak of the original instance of the Message content to the Receiver, thus insuring the Receiver must decrypt the message to have access to the original plain text Message content.
    }
}
