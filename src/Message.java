//Charles Justin Reusnow • CS 2261-001

//Imports
import java.util.Random;

//The Message object securely stores the content, encrypted content, and decrypted content within itself. It facilitates the encryption and decryption process, as well as the key generation and verification process.
class Message {
    //Final Variables
    private static final Random RANDOM = new Random(System.nanoTime()); //The random number generator. Seeded with System.nanoTime() to ensure every run is different.
    private static final char[] VOWELS = {'a', 'e', 'i', 'o', 'u'}; //And sometimes 'y'.
    private static final char[] REMOVABLE_CHARACTERS = {'!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@'}; //These are the special characters. This variable is used to remove additional punctuation from the message.

    //Member Variables
    private String content; //The content of the message, as the Communicator first had it (Plain text for Senders, encrypted for Receivers).
    private String encryptedContent; //Used by the Sender to store the encrypted Message content.
    private String decryptedContent; //Used by the Receiver to store the decrypted Message content.

    //Constructor
    public Message(String content) {
        this.content = content;
        this.encryptedContent = null; //Will be populated by Sender later.
        this.decryptedContent = null; //Will be populated by Receiver later.
    }

    //Getters
    public String getContent() {
        return this.content;
    }

    public String getEncryptedContent() {
        return this.encryptedContent;
    }

    public String getDecryptedContent() {
        return this.decryptedContent;
    }

    //Setters
    public void setContent(String content) {
        this.content = content;
    }

    public void setEncryptedContent(String encryptedContent) {
        this.encryptedContent = encryptedContent;
    }

    public void setDecryptedContent(String decryptedContent) {
        this.decryptedContent = decryptedContent;
    }

    //ToString
    public String toString() {
        return "Message{" +
                "content='" + content + '\'' +
                ", encryptedContent='" + encryptedContent + '\'' +
                ", decryptedContent='" + decryptedContent + '\'' +
                '}';
    }

    //Member Methods
    //Generates the encryption key as per the instructions in the Project 3 PDF.
    String generateKey() {
        char[] X; //Stores the alphabetical characters for the encryption key.
        char[] Y; //Stores the special characters for the encryption key.
        char Z; //Stores the random number between 2 and 5 inclusive for the encryption key.
        int curChar; //Used in generation of all X, Y, and Z to make sure the characters are correct. Its an iterator basically.

        //X CHARACTER GENERATOR
        char[][] alphabetCharRanges = {{'A', 'Z'}, {'a', 'z'}}; //Alphabetical characters fall within these two ranges.
        int numAlphabetChars = alphabetCharRanges.length; //The number of alphabetical characters in those two ranges (plus the number of ranges. Not sure why, but testing has proven this to be necessary).
        for (char[] alphabetCharRange : alphabetCharRanges) { //For every range of characters in the set of alphabetical character ranges.
            numAlphabetChars += alphabetCharRange[1] - alphabetCharRange[0]; //The number of alphabetical characters is what it was plus the number of characters in this range.
        }
        X = new char[]{(char) (RANDOM.nextDouble() * (numAlphabetChars)), (char) (RANDOM.nextDouble() * (numAlphabetChars)), (char) (RANDOM.nextDouble() * (numAlphabetChars)), (char) (RANDOM.nextDouble() * (numAlphabetChars))}; //Generate 4 random characters between 0 and the number of alphabetical characters possible.
        curChar = 0; //Reset the iterator.
        for (char x : X) { //For every X character in X...
            if (x < ('Z' - 'A' + 1)) { //If the random character is between 0 and 25 inclusive ('A' to 'Z')
                X[curChar] += 'A'; //Offset it with 'A' so the character @ 0 => 0 + 'A' => 'A"; @ 1 => 1 + 'A' => 'B'; etc.
            } else { //If it falls elsewhere (so in the 'a' to 'z' range)...
                X[curChar] += 'A' + (('a' - 1) - 'Z'); //Offset it with 'A' + the difference between the first letter of the next set, minus the last letter of the previous set, so the character @ 26 => 26 + 'A' + [the gap between 'Z' and 'a'] => 'a'; etc.
            }
            curChar++; //Iterate to keep track of the characters.
        }

        //Y CHARACTER GENERATOR
        int[][] specialCharRanges = {{'!', '/'}, {':', '@'}}; //Special characters fall within these two ranges.
        int numSpecialChars = specialCharRanges.length; //The process is exactly the same as the X Generator from here on, just with different characters, and different amounts of characters generated.
        for (int[] specialCharRange : specialCharRanges) {
            numSpecialChars += specialCharRange[1] - specialCharRange[0];
        }
        Y = new char[]{(char) (RANDOM.nextDouble() * numSpecialChars), (char) (RANDOM.nextDouble() * numSpecialChars)};
        curChar = 0;
        for (char y : Y) {
            if (y < ('/' - '!' + 1)) {
                Y[curChar] += '!';
            } else {
                Y[curChar] += '!' + ((':' - 1) - '/');
            }
            curChar++;
        }

        //Z CHARACTER GENERATOR
        int[][] numberCharRanges = {{'2', '5'}}; //The specified numeric characters fall within these ranges (this doesn't have to be a 2D array, but for consistency I did it anyway).
        int numNumberChars = numberCharRanges.length;
        for (int[] numberCharRange : numberCharRanges) {
            numNumberChars += numberCharRange[1] - numberCharRange[0];
        }
        Z = (char) (RANDOM.nextDouble() * numNumberChars);
        Z += '2'; //Offset the chracter with '2', so @ 0 => 0 + '2' => '2'; @ 1 => 1 + '2' => '3'; etc.

        return "" + X[0] + X[1] + "-" + Y[0] + X[2] + X[3] + "-" + Z + Y[1]; //Return the key assembled in the correct order.
    }

    //Encrypts the Message content via the encryption key as per the instructions in the Project 3 PDF.
    void encrypt(String key) {
        String trimmedContent = this.content.trim(); //Trim the leasing and trailing spaces from the content.
        String intermediaryContent = ""; //This variable is used through the various permutations of the content during the encryption process.

        //ENCRYPTION STEP 1
        //NOTE: Assuming 'punctuation' means the same thing as 'special characters', meaning of course ',' would be trimmed, but also '$' and '@' and even '%' will be trimmed as well (unless it is the last character of the content).
        for (int i = 0; i < trimmedContent.length() - 1; i++) { //For every character in the trimmed version of the content except the last...
            boolean goodChar = true; //Assume this character is good.
            for (char removableChar : REMOVABLE_CHARACTERS) { //For every character in the set of bad characters...
                if (trimmedContent.charAt(i) == removableChar) { //If the current character is a bad character...
                    goodChar = false; //This character is not good.
                }
            }

            if (goodChar) { //If the current character was good...
                intermediaryContent += trimmedContent.charAt(i); //Include this character in the encryption process.
            }
        }

        intermediaryContent += trimmedContent.charAt(trimmedContent.length() - 1); //Throw the last character on to the end (The last character will always be a good character, since we do not care what it is).

        //ENCRYPTION STEP 2
        intermediaryContent = intermediaryContent.toLowerCase(); //Make it lowercase.

        //ENCRYPTION STEP 3
        String[] intermediaryWords = intermediaryContent.split(" "); //Split the content into words based on occurrences of spaces.
        intermediaryContent = ""; //Reset intermediary content so we can rebuild it after encrypting the words individually.

        for (int i = 0; i < intermediaryWords.length; i++) { //For each word that was split off...
            //ENCRYPTION STEP 3.1
            if (intermediaryWords[i].length() > 0) { //If this word is longer than one character (prevents crashing when consecutive spaces are found in the Message content)...
                char firstChar = intermediaryWords[i].charAt(0); //Get the first char of the current word.
                boolean isConsonant = true; //Assume it is a consonant.

                for (char vowel : VOWELS) { //For every vowel in the VOWELS variable...
                    if (firstChar == vowel) { //If the current character is a vowel...
                        isConsonant = false; //This character is not a consonant.
                    }
                }

                if (isConsonant) { //If this character is a consonant...
                    intermediaryWords[i] = intermediaryWords[i].substring(1) + firstChar + key.charAt(0) + key.charAt(1); //This word = the word except the first character + the first character at the end + X1.X2 of the encryption key at the end.
                } else { //If this character is a vowel.
                    intermediaryWords[i] += "" + key.charAt(3) + key.charAt(4) + key.charAt(5); //This word = the word + Y1.X3.X4 of the encryption key at the end.
                }

                intermediaryContent += intermediaryWords[i] + " "; //Append the newly encrypted word back into the intermediary content variable plus a space.
            }
        }
        intermediaryContent = intermediaryContent.trim(); //Trim off the last space.

        //ENCRYPTION STEP 3.2
        int multiple = Character.getNumericValue(key.charAt(7)); //Get Z from the encryption key.
        for (int i = multiple - 1; i < intermediaryContent.length(); i += multiple) { //For every Zth character in the intermediary content variable...
            intermediaryContent = intermediaryContent.substring(0, i) + key.charAt(8) + intermediaryContent.substring(i); //Insert Y2 from the encryption key into the string at this location.
        }

        this.encryptedContent = intermediaryContent; //Encryption is now complete, so store the intermediary content variable back in the Message object.
    }

    //Checks that the specific encryption key matches the general pattern encryption keys are created to follow.
    boolean verifyKey(String key) {
        /* THE WAY THIS WORKS:
        ^ = From the beginning of the string.
        $ = From the end of the string (using these two together means the whole string must match).
        X1.X2 => ([A-Z]|[a-z]){2} = First two characters must be a character between A and Z OR a character between a and z.
        - = The next character must be a dash.
        Y1 => ([!-/]|[:-@]) = The next character must be between ! and /, or between : and @.
        X3.X4 => ([A-Z]|[a-z]){2} = The next two characters must be a character between A and Z OR a character between a and z.
        - = The next character must be a dash.
        Z => [2-5] = The next chanter must be between 2 and 5.
        Y2 => ([!-/]|[:-@]) = The next character must be between ! and /, or between : and @. */
        return key.matches("^([A-Z]|[a-z]){2}-([!-/]|[:-@])([A-Z]|[a-z]){2}-[2-5]([!-/]|[:-@])$");
    }

    //Decrypts the content based on the encryption key.
    void decrypt(String key) {
        String intermediaryContent = this.content; //This variable is used through the various permutations of the content during the decryption process.

        //DECRYPTION STEP 3.2
        char removeMe = key.charAt(8); //Get Y2 from the encryption key.
        while (intermediaryContent.indexOf(removeMe) != -1) { //While there are more instances of the character that needs to be removed...
            intermediaryContent = intermediaryContent.substring(0, intermediaryContent.indexOf(removeMe)) + intermediaryContent.substring(intermediaryContent.indexOf(removeMe) + 1); //Substring that character out.
        }

        //DECRYPTION 3
        String[] intermediaryWords = intermediaryContent.split(" "); //Split the content into words based on the occurrences of spaces.
        intermediaryContent = ""; //Reset the intermediary content variable so we can decrypt each word then load it back into this variable.
        for (int i = 0; i < intermediaryWords.length; i++) { //For each word found in the intermediary content variable...
            //DECRYPTION 3.1
            if (intermediaryWords[i].substring(intermediaryWords[i].length() - 3).equals("" + key.charAt(3) + key.charAt(4) + key.charAt(5))) { //If the last 3 characters of the word are Y1.X3.X4 from the encryption key.
                intermediaryWords[i] = intermediaryWords[i].substring(0, intermediaryWords[i].length() - 3); //This word starts with a vowel, so simply substring those three characters out.
            } else { //If the last 3 characters of the word are not Y1.X3.X4 from the encryption key...
                intermediaryWords[i] = intermediaryWords[i].charAt(intermediaryWords[i].length() - 3) + intermediaryWords[i].substring(0, intermediaryWords[i].length() - 3); //This word starts wit ha consonant, so copy third to last character and put it at thr front, then cut off the last three characters.
            }
            intermediaryContent += intermediaryWords[i] + " "; //Append the newly decrypted word back into the intermediary content variable plus a space..
        }
        intermediaryContent = intermediaryContent.trim(); //Trim off the last space.

        //DECRYPTION STEP 2
        //Instructed to leave the decrypted message lowercase.

        //DECRYPTION STEP 1
        //Instructed to leave the excess punctuation off in the decryption.

        this.decryptedContent = intermediaryContent; //Decryption is now complete, so store the intermediary content variable back in the Message object.
    }
}